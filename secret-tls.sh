#!/bin/bash
# $1 namespace

kubectl create secret tls siscc-cert --cert <name>.crt --key <name>.key -n $1
