#!/bin/bash
# $1 namespace

kubectl create configmap authz --from-env-file=.env.authz --namespace=$1
kubectl create configmap mssql --from-env-file=.env.mssql --namespace=$1