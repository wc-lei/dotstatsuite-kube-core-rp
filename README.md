# Content of this repository
This repository contains the kubernetes strategies of the *.stat-suite core components*, for the OECD's *qa* and *staging* environments.  
These environments are used for the **development** of the .stat-suite.  
The content of this repository is divided into 2 folders *qa* and *staging*, each one with yaml files (to describe the strategy of each service) and .env files (to store the configuration values).

These files can be used as an example to deploy the .stat-suite core, using kubernetes. 

> **Please note that the configuration values used in this examples are public, therefore leaving the configuration values as they are, is a security threat for production deployments.**

# Quickstart
```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd-core
gcloud config set compute/region europe-west1
gcloud container clusters get-credentials oecd-core --region europe-west1-b
kubectl get pods -n qa -o wide
kubectl get pods -n staging -o wide
```

# Setup
> mssql password must be at least 8 characters long and contain characters from three of the following four sets: Uppercase letters, Lowercase letters, Base 10 digits, and Symbols.

1. create global IP:
  - run `gcloud compute addresses create oecd-core-<namespace>-static-ip --global`
  - check with `gcloud compute addresses describe oecd-core-<namespace>-static-ip --global`
1. kubectl create -f namespace.json
1. create docker regcred secret from `regcred.sh`
1. create secrets
  - create an executable shell file (chmod +x)
  - `kubectl create secret generic <secret> --from-literal=password="password" --namespace=<namespace>`
  - execute and remove it
1. create configmaps from .env files:
  - `kubectl create configmap <name> --from-env-file=<filepath> --namespace=<namespace>`
1. run `kubectl apply -f <strategy>.yaml` (--dry-run)

# Misc
- initContainers logs: `kubectl logs <pod-name-hash> -c <init-container-name>`
- update a configmap requires to restart underlying deployments: `kubectl rollout restart deployment -n qa  nsi-stable`
- get log file: `kubectl logs <pod-name-hash> > <pod-name>.log`
- exec a container: `kubectl exec -it -n qa transfer-8c49c5d7f-ds6d6 -- /bin/bash`

# Probes
- readiness: used to know when a service has started successfully and is ready to serve `/health`
- liveness: used to restart a service if a broken state is reached `/live`

*note: `/live is not implemented for nsi service`*

# backup & restore

## snapshot (backup)
**1 create a snapshot schedule**
```
gcloud compute resource-policies create snapshot-schedule schedule-mssql-staging-0 \
  --project=oecd-228113 \
  --region=europe-west1 \
  --max-retention-days=14 \
  --on-source-disk-delete=keep-auto-snapshots \
  --daily-schedule \
  --start-time=03:00 \
  --storage-location=europe-west1
```

**2 identify the disk**
- in GKE > Clusters
- choose the cluster (ie oecd-core) > Storage tab
- in the list of persistent volumes, search the claim to snapshot (ie used by mssql)
- the source is the name of the disk

**3 attach the snapshot schedule to the disk**
- in Compute Engine > Disks
- select the disk
- edit and select the snapshot schedule

## restore a snapshot
**1 create a disk from a snapshot**
```
gcloud beta compute disks create my-new-disk \
  --project=oecd-228113 \
  --type=pd-standard \
  --size=50GB \
  --zone=europe-west1-b \
  --source-snapshot=mssql-snapshot
```

**2 bind the disk to a persistent volume claim through a persistent volume**
> a pvc is a powerful abstract of kubernetes to have reusable strategies over different cloud providers.  
> a pvc (with a storage class) is enough to setup persistency for a service (ie mssql).  
> a pvc will create underlying pv and disk.  
> when there's already a disk to use, the pv needs to be explicit.

*first, create a file temp-pv-pvc.yaml:*
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-persistent-volume
  namespace: staging
  labels:
    app: restore
spec:
  capacity:
    storage: 50Gi # should not be above disk size
  accessModes:
    - ReadWriteOnce
  gcePersistentDisk:
    pdName: my-new-disk # should be the name of the new disk
    fsType: ext4
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-new-persistent-volume-claim
  namespace: staging
  labels:
    app: restore
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: standard
  resources:
    requests:
      storage: 50Gi
  selector:
    matchLabels:
      app: restore
```

*then, apply the file:* `kubectl apply -f temp-pv-pvc.yml`

**3 update existing file (ie mssql.yaml) to use the new pvc and apply it (pvc definition and pvc usage in deployment)**
